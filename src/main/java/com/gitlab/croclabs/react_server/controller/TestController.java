package com.gitlab.croclabs.react_server.controller;

import com.gitlab.croclabs.react_server.dtos.TestDto;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TestController {
	private final HttpServletResponse response;

	@GetMapping("/test")
	public TestDto test() {
		return TestDto.builder().test2("2").build();
	}

	@GetMapping("/test404")
	public TestDto test404() {
		response.setStatus(404);
		return TestDto.builder().test2("2").build();
	}

	@GetMapping("/test/lazy")
	public TestDto testLazy() throws InterruptedException {
		Thread.sleep(3000);
		return TestDto.builder().test1("Lazy done!").test2("2").build();
	}

	@PostMapping("/test")
	public TestDto testPost() {
		return TestDto.builder().test2("2").build();
	}
}
