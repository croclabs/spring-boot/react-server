package com.gitlab.croclabs.react_server.controller;

import com.gitlab.croclabs.react_server.db.user.User;
import com.gitlab.croclabs.react_server.db.user.UserDto;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/auth")
public class AuthController {
	@GetMapping("/current")
	public UserDto currentUser() {
		AtomicReference<UserDto> userDto = new AtomicReference<>(null);
		User.currentOpt().ifPresent(user -> userDto.set(user.toDto()));
		return userDto.get();
	}
}
