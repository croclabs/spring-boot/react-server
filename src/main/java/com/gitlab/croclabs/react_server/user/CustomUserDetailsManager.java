package com.gitlab.croclabs.react_server.user;

import com.gitlab.croclabs.react_server.db.user.User;
import com.gitlab.croclabs.react_server.db.user.UserDao;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@RequiredArgsConstructor
public class CustomUserDetailsManager implements UserDetailsService {
	private final UserDao dao;

	@Override
	public User loadUserByUsername(String username) throws UsernameNotFoundException {
		if (!dao.existsByUsername(username)) {
			throw new UsernameNotFoundException("not found");
		}

		return dao.findByUsername(username);
	}
}
