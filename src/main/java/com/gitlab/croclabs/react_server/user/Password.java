package com.gitlab.croclabs.react_server.user;

import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class Password {
	private final PasswordEncoder pe;

	public String make(String password) {
		return pe.encode(password);
	}
}
