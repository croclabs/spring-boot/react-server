package com.gitlab.croclabs.react_server.dtos;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TestDto {
	@TSOptional
	String test1;
	String test2;
}
