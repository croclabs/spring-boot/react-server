package com.gitlab.croclabs.react_server;

import com.gitlab.croclabs.react_server.db.user.User;
import com.gitlab.croclabs.react_server.db.user.UserDao;
import com.gitlab.croclabs.react_server.login.AuthenticationFailureHandler;
import com.gitlab.croclabs.react_server.login.AuthenticationSuccessHandler;
import com.gitlab.croclabs.react_server.user.CustomUserDetailsManager;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.data.domain.AuditorAware;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.HeadersConfigurer.FrameOptionsConfig;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Configuration
@Log4j2
public class SecurityConfig implements AuditorAware<UUID> {
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		return http
				.authorizeHttpRequests(config ->
						config
								.requestMatchers(
										PathRequest.toH2Console(),
										new AntPathRequestMatcher("/login"),
										new AntPathRequestMatcher("/static/**"),
										new AntPathRequestMatcher("/favicon.ico"),
										new AntPathRequestMatcher("/asset-manifest.json"),
										new AntPathRequestMatcher("/manifest.json"),
										new AntPathRequestMatcher("/logo192.png"),
										new AntPathRequestMatcher("/logo512.png")
								).permitAll()
								.anyRequest().authenticated()
				)
				.formLogin(conf -> conf
						.loginPage("/login")
						.failureHandler(new AuthenticationFailureHandler())
						.successHandler(new AuthenticationSuccessHandler())
				)
				.csrf(config -> config.ignoringRequestMatchers(PathRequest.toH2Console()))
				.headers(config -> config.frameOptions(FrameOptionsConfig::sameOrigin))
				.build();
	}

	@Bean
	public UserDetailsService userDetailsService(UserDao userDao, PasswordEncoder pe, Environment env) {
		if (env.containsProperty("debug.create.users") &&
				!"false".equalsIgnoreCase(env.getProperty("debug.create.users"))) {
			createDebugUsers(userDao, pe);
		}

		return new CustomUserDetailsManager(userDao);
	}

	@Bean
	public PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}

	private static void createDebugUsers(UserDao userDao, PasswordEncoder pe) {
		User user = User.builder()
				.username("debug")
				.authorities(new HashSet<>(Set.of(
						new SimpleGrantedAuthority("USER"),
						new SimpleGrantedAuthority("DEBUG")
				)))
				.password(pe.encode("debug"))
				.firstname("debug")
				.lastname("debug")
				.build();

		userDao.create();
		userDao.insert(user);
		log.info(
				"""


				Debug login: "debug" and password "debug"
				Deactivate this feature by property "debug.create.users"
				"""
		);
	}

	@Override
	public Optional<UUID> getCurrentAuditor() {
		return Optional.ofNullable(User.currentOpt().orElse(new User()).getId());
	}
}
