package com.gitlab.croclabs.react_server.login;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.croclabs.react_server.db.user.User;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;

import java.io.IOException;

public class AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	public AuthenticationSuccessHandler() {
		super();
		setRedirectStrategy(new NoRedirectStrategy());
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
	                                    Authentication authentication) throws IOException, ServletException {

		super.onAuthenticationSuccess(request, response, authentication);
		ObjectMapper mapper = new ObjectMapper();

		response.setContentType("application/json;charset=UTF-8");
		response.getWriter().print(mapper.writeValueAsString(((User) authentication.getPrincipal()).toDto()));
		response.getWriter().flush();
	}
}
