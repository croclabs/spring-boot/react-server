package com.gitlab.croclabs.react_server;

import com.gitlab.croclabs.react_server.db.user.User;
import com.gitlab.croclabs.react_server.db.user.UserDao;
import com.gitlab.croclabs.react_server.user.Password;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import java.io.IOException;

@Controller
@RequiredArgsConstructor
@Log4j2
public class AppController implements ErrorController {
	private final HttpServletResponse response;
	private final UserDao userDao;
	private final Password pw;

	@GetMapping("/")
	public String index() {
		log.info("current user: " + User.current());
		User.currentOpt().ifPresent(user -> {
			log.info("db user: " + userDao.findById(user.getId()));
			user.setFirstname("debug_new");
			user.setVersion(userDao.update(user));
			log.info("db user: " + userDao.findById(user.getId()));
			log.info("current user: " + User.current());
		});

		return "index";
	}

	@GetMapping("/login")
	public String login() {
		return "index";
	}

	@GetMapping("/error")
	public String error() throws IOException {
		return switch (response.getStatus()) {
			case 404 -> {
				response.setStatus(200);
				yield "index";
			}
			case 200 -> "redirect:/";
			case 500 -> "redirect:/error/500";
			default -> "redirect:/login";
		};
	}
}
