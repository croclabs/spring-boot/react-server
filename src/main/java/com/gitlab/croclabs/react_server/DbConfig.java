package com.gitlab.croclabs.react_server;

import com.gitlab.croclabs.react_server.db.CreateDao;
import com.gitlab.croclabs.react_server.db.SqlLoggerImpl;
import lombok.extern.log4j.Log4j2;
import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.argument.ArgumentFactory;
import org.jdbi.v3.core.mapper.ColumnMapper;
import org.jdbi.v3.core.mapper.ColumnMapperFactory;
import org.jdbi.v3.core.mapper.RowMapper;
import org.jdbi.v3.core.mapper.RowMapperFactory;
import org.jdbi.v3.core.spi.JdbiPlugin;
import org.jdbi.v3.sqlobject.SqlObjectPlugin;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy;

import javax.sql.DataSource;
import java.util.List;

@Configuration
@Log4j2
public class DbConfig {
	@Bean
	public Jdbi jdbi(
			Environment environment,
			DataSource ds,
			List<JdbiPlugin> jdbiPlugins,
			List<RowMapper<?>> rowMappers,
			List<ColumnMapper<?>> columnMappers,
			List<RowMapperFactory> rowMapperFactories,
			List<ArgumentFactory> argumentFactories,
			List<ColumnMapperFactory> columnMapperFactories
	) {
		TransactionAwareDataSourceProxy proxy = new TransactionAwareDataSourceProxy(ds);
		Jdbi jdbi = Jdbi.create(proxy);

		// Register all available plugins
		log.info("Installing plugins... ({} found)", jdbiPlugins.size());
		jdbiPlugins.forEach(jdbi::installPlugin);

		// Register all available rowMappers
		log.info("Installing rowMappers... ({} found)", rowMappers.size());
		rowMappers.forEach(jdbi::registerRowMapper);

		// Register all available rowMappers
		log.info("Installing columnMappers... ({} found)", columnMappers.size());
		columnMappers.forEach(jdbi::registerColumnMapper);

		log.info("Installing rowMapperFactories... ({} found)", rowMapperFactories.size());
		rowMapperFactories.forEach(jdbi::registerRowMapper);

		log.info("Installing argumentFactories... ({} found)", argumentFactories.size());
		argumentFactories.forEach(jdbi::registerArgument);

		log.info("Installing columnMapperFactories... ({} found)", columnMapperFactories.size());
		columnMapperFactories.forEach(jdbi::registerColumnMapper);

		jdbi.setSqlLogger(new SqlLoggerImpl("true".equals(environment.getProperty("debug.sql"))));

		return jdbi;
	}

	@Bean
	public List<CreateDao> createDaos(List<CreateDao> daos) {
		daos.forEach(CreateDao::create);
		return daos;
	}

	@Bean
	public SqlObjectPlugin sqlObjectPlugin() {
		return new SqlObjectPlugin();
	}
}
