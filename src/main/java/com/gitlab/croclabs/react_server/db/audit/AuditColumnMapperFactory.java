package com.gitlab.croclabs.react_server.db.audit;

import org.jdbi.v3.core.config.ConfigRegistry;
import org.jdbi.v3.core.mapper.ColumnMapper;
import org.jdbi.v3.core.mapper.ColumnMapperFactory;

import java.lang.reflect.Type;
import java.util.Optional;

public class AuditColumnMapperFactory implements ColumnMapperFactory {
	@Override
	public Optional<ColumnMapper<?>> build(Type type, ConfigRegistry config) {
		if (type == TimeAudit.class) {
			return Optional.of(TimeAudit::new);
		}

		if (type == UserAudit.class) {
			return Optional.of(UserAudit::new);
		}

		return Optional.empty();
	}
}
