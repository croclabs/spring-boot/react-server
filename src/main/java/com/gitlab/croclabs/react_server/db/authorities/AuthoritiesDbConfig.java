package com.gitlab.croclabs.react_server.db.authorities;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuthoritiesDbConfig {

	@Bean
	public AuthoritiesArgumentFactory authoritiesArgumentFactory() {
		return new AuthoritiesArgumentFactory();
	}

	@Bean
	public AuthoritiesColumnMapper authoritiesColumnMapper() {
		return new AuthoritiesColumnMapper();
	}
}
