package com.gitlab.croclabs.react_server.db.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.gitlab.croclabs.react_server.db.audit.TimeAudit;
import com.gitlab.croclabs.react_server.db.audit.UserAudit;
import lombok.*;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.Serial;
import java.util.*;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class User implements UserDetails {
	@Serial
	private static final long serialVersionUID = -9066031410709052518L;

	private UUID id;

	private String firstname;
	private String lastname;

	@ToString.Exclude
	@JsonIgnore
	private String password;

	private String username;
	private Set<GrantedAuthority> authorities;

	@Builder.Default
	private boolean accountNonExpired = true;
	@Builder.Default
	private boolean accountNonLocked = true;
	@Builder.Default
	private boolean credentialsNonExpired = true;
	@Builder.Default
	private boolean enabled = true;

	@Builder.Default
	private UserAudit createdBy = new UserAudit();
	@Builder.Default
	private UserAudit updatedBy = new UserAudit();
	@Builder.Default
	@JsonIgnore
	private TimeAudit created = new TimeAudit();
	@Builder.Default
	@JsonIgnore
	private TimeAudit updated = new TimeAudit();
	@Builder.Default
	private Long version = 1L;

	public User(String username, String password, Collection<? extends GrantedAuthority> authorities) {
		this.username = username;
		this.password = password;
		this.authorities = new HashSet<>(authorities);
	}

	public static User current() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();

		if (auth == null) {
			return null;
		}

		return (User) auth.getPrincipal();
	}

	public static Optional<User> currentOpt() {
		return Optional.ofNullable(current());
	}

	public UserDto toDto() {
		return new UserDto(this);
	}
}
