package com.gitlab.croclabs.react_server.db.authorities;

import org.jdbi.v3.core.argument.AbstractArgumentFactory;
import org.jdbi.v3.core.argument.Argument;
import org.jdbi.v3.core.argument.ArgumentFactory;
import org.jdbi.v3.core.config.ConfigRegistry;
import org.springframework.security.core.GrantedAuthority;

import java.sql.Types;
import java.util.Set;

public class AuthoritiesArgumentFactory extends AbstractArgumentFactory<Set<GrantedAuthority>> {
	/**
	 * Constructs an {@link ArgumentFactory} for type {@code T}.
	 *
	 */
	public AuthoritiesArgumentFactory() {
		super(Types.ARRAY);
	}

	@Override
	protected Argument build(Set<GrantedAuthority> value, ConfigRegistry config) {
		return (position, statement, ctx) -> statement.setArray(
				position,
				ctx.getConnection().createArrayOf(
						"VARCHAR",
						value.stream().map(GrantedAuthority::getAuthority).toArray()
				)
		);
	}
}
