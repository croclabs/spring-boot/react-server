package com.gitlab.croclabs.react_server.db.audit;

import org.jdbi.v3.core.argument.Argument;

public interface Audit<T> {
	T getAudit();
	void setAudit(T audit);
	Argument toArgument();
}
