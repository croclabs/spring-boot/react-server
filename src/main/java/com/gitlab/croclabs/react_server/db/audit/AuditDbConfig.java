package com.gitlab.croclabs.react_server.db.audit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AuditDbConfig {
	@Bean
	public AuditArgumentFactory auditArgumentFactory() {
		return new AuditArgumentFactory();
	}

	@Bean
	public AuditColumnMapperFactory auditColumnMapperFactory() {
		return new AuditColumnMapperFactory();
	}
}
