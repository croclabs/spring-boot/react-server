package com.gitlab.croclabs.react_server.db.authorities;

import org.jdbi.v3.core.mapper.ColumnMapper;
import org.jdbi.v3.core.statement.StatementContext;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class AuthoritiesColumnMapper implements ColumnMapper<Set<GrantedAuthority>> {
	@Override
	public Set<GrantedAuthority> map(ResultSet r, int columnNumber, StatementContext ctx) throws SQLException {
		return Arrays.stream((Object[]) r.getArray(columnNumber).getArray())
				.map(o -> o instanceof String ? (String) o : null)
				.filter(Objects::nonNull)
				.map(SimpleGrantedAuthority::new)
				.collect(Collectors.toSet());
	}
}
