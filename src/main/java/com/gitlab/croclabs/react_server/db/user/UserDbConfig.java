package com.gitlab.croclabs.react_server.db.user;

import org.jdbi.v3.core.Jdbi;
import org.jdbi.v3.core.mapper.RowMapperFactory;
import org.jdbi.v3.core.mapper.reflect.FieldMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UserDbConfig {
	@Bean
	public UserDao userDao(Jdbi jdbi) {
		return jdbi.onDemand(UserDao.class);
	}

	@Bean
	public RowMapperFactory userRowMapper() {
		return FieldMapper.factory(User.class);
	}
}
