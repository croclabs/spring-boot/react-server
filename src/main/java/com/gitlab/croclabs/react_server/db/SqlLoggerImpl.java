package com.gitlab.croclabs.react_server.db;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.jdbi.v3.core.statement.SqlLogger;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.SQLException;
import java.time.temporal.ChronoUnit;

@Log4j2
@RequiredArgsConstructor
public class SqlLoggerImpl implements SqlLogger {
	private final boolean debug;

	@Override
	public void logAfterExecution(StatementContext context) {
		if (debug) {
			log.info(
					"""
							SQL QUERY:
							{}
							{}ms
							""",
					context.getStatement(),
					context.getElapsedTime(ChronoUnit.MILLIS)
			);
		}
	}

	@Override
	public void logException(StatementContext context, SQLException ex) {
		log.info(
				"""
				SQL QUERY:
				{}
				{}
				""",
				context.getStatement(),
				ex
		);
	}
}
