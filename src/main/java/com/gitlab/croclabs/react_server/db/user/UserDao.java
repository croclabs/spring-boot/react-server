package com.gitlab.croclabs.react_server.db.user;

import com.gitlab.croclabs.react_server.db.CreateDao;
import org.jdbi.v3.sqlobject.customizer.BindBean;
import org.jdbi.v3.sqlobject.locator.UseClasspathSqlLocator;
import org.jdbi.v3.sqlobject.statement.GetGeneratedKeys;
import org.jdbi.v3.sqlobject.statement.SqlQuery;
import org.jdbi.v3.sqlobject.statement.SqlUpdate;

import java.util.UUID;

@UseClasspathSqlLocator
public interface UserDao extends CreateDao {
	@SqlUpdate
	void create();

	@SqlUpdate
	@GetGeneratedKeys("id")
	UUID insert(@BindBean User user);

	@SqlUpdate
	@GetGeneratedKeys("version")
	Long update(@BindBean User user);

	@SqlQuery
	User findById(UUID id);

	@SqlQuery
	boolean existsByUsername(String username);

	@SqlQuery
	User findByUsername(String username);
}
