package com.gitlab.croclabs.react_server.db.audit;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jdbi.v3.core.argument.Argument;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Objects;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TimeAudit implements Audit<Instant> {
	private Instant audit;

	public TimeAudit(ResultSet r, int columnNumber, StatementContext ctx) throws SQLException {
		Timestamp timestamp = r.getTimestamp(columnNumber);

		if (timestamp != null) {
			audit = timestamp.toInstant();
		}
	}

	@Override
	public Argument toArgument() {
		return (position, statement, ctx) -> {
			audit = Instant.now();
			statement.setTimestamp(position, Timestamp.from(audit));
		};
	}

	@Override
	public String toString() {
		return Objects.requireNonNullElse(audit, "null").toString();
	}
}
