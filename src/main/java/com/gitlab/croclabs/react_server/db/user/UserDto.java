package com.gitlab.croclabs.react_server.db.user;

import com.gitlab.croclabs.react_server.dtos.TSOptional;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serial;
import java.io.Serializable;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

@Data
@RequiredArgsConstructor
@AllArgsConstructor
public class UserDto implements Serializable {
	@Serial
	private static final long serialVersionUID = 2422346442618465697L;

	private final UUID id;
	private final String username;

	@TSOptional
	private String firstname;
	@TSOptional
	private String lastname;

	@TSOptional
	private Set<String> authorities;

	@TSOptional
	private Boolean accountNonExpired;
	@TSOptional
	private Boolean accountNonLocked;
	@TSOptional
	private Boolean credentialsNonExpired;
	@TSOptional
	private Boolean enabled;

	public UserDto(User user) {
		this.id = user.getId();
		this.username = user.getUsername();
		this.firstname = user.getFirstname();
		this.lastname = user.getLastname();
		this.accountNonExpired = user.isAccountNonExpired();
		this.accountNonLocked = user.isAccountNonLocked();
		this.credentialsNonExpired = user.isCredentialsNonExpired();
		this.enabled = user.isEnabled();
		this.authorities = user.getAuthorities().stream()
				.map(GrantedAuthority::getAuthority)
				.collect(Collectors.toSet());
	}
}
