package com.gitlab.croclabs.react_server.db.audit;

import com.gitlab.croclabs.react_server.db.user.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jdbi.v3.core.argument.Argument;
import org.jdbi.v3.core.statement.StatementContext;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicReference;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserAudit implements Audit<UUID> {
	private UUID audit;

	public UserAudit(ResultSet r, int columnNumber, StatementContext ctx) throws SQLException {
		String uuid = r.getString(columnNumber);

		if (uuid != null) {
			audit = UUID.fromString(uuid);
		}
	}

	public Argument toArgument() {
		return (position, statement, ctx) -> {
			AtomicReference<String> strId = new AtomicReference<>(null);
			User.currentOpt().ifPresent(user -> {
				audit = user.getId();
				strId.set(audit.toString());
			});
			statement.setString(position, strId.get());
		};
	}

	@Override
	public String toString() {
		return Objects.requireNonNullElse(audit, "null").toString();
	}
}
