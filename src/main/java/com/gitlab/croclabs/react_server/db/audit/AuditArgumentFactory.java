package com.gitlab.croclabs.react_server.db.audit;

import org.jdbi.v3.core.argument.Argument;
import org.jdbi.v3.core.argument.ArgumentFactory;
import org.jdbi.v3.core.config.ConfigRegistry;

import java.lang.reflect.Type;
import java.util.Optional;

public class AuditArgumentFactory implements ArgumentFactory {
	@Override
	public Optional<Argument> build(Type type, Object value, ConfigRegistry config) {
		if (value instanceof Audit<?>) {
			return Optional.of(((Audit<?>) value).toArgument());
		}

		return Optional.empty();
	}
}
