UPDATE user_account
SET
    username = :username,
    firstname = :firstname,
    lastname = :lastname,
    authorities = :authorities,
    password = :password,
    account_non_expired = :accountNonExpired,
    account_non_locked = :accountNonLocked,
    credentials_non_expired = :credentialsNonExpired,
    enabled = :enabled,
    updated = :updated,
    updated_by = :updatedBy,
    version = version + 1
WHERE
    id = :id