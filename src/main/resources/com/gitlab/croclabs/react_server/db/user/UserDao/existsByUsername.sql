SELECT
    CASE WHEN EXISTS (
        SELECT * FROM user_account WHERE username = :username
    )
    THEN 'true'
    ELSE 'false'
    END
