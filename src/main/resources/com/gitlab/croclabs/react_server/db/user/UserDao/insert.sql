INSERT INTO user_account (
    firstname,
    lastname,
    password,
    username,
    account_non_expired,
    account_non_locked,
    credentials_non_expired,
    enabled,
    authorities,
    created,
    created_by
) VALUES (
    :firstname,
    :lastname,
    :password,
    :username,
    :accountNonExpired,
    :accountNonLocked,
    :credentialsNonExpired,
    :enabled,
    :authorities,
    :created,
    :createdBy
)