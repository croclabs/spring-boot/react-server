/* tslint:disable */
/* eslint-disable */
// Generated using typescript-generator version 3.2.1263 on 2023-10-08 13:17:47.

export interface UserDto extends Serializable {
    id: string;
    username: string;
    firstname?: string;
    lastname?: string;
    authorities?: string[];
    accountNonExpired?: boolean;
    accountNonLocked?: boolean;
    credentialsNonExpired?: boolean;
    enabled?: boolean;
}

export interface TestDto {
    test1?: string;
    test2: string;
}

export interface Serializable {
}

export interface HttpClient {

    request<R>(requestConfig: { method: string; url: string; queryParams?: any; data?: any; copyFn?: (data: R) => R; }): RestResponse<R>;
}

export class RestApplicationClient {

    constructor(protected httpClient: HttpClient) {
    }

    /**
     * HTTP GET /auth/current
     * Java method: com.gitlab.croclabs.react_server.controller.AuthController.currentUser
     */
    currentUser(): RestResponse<UserDto> {
        return this.httpClient.request({ method: "GET", url: uriEncoding`auth/current` });
    }

    /**
     * HTTP GET /test
     * Java method: com.gitlab.croclabs.react_server.controller.TestController.test
     */
    test(): RestResponse<TestDto> {
        return this.httpClient.request({ method: "GET", url: uriEncoding`test` });
    }

    /**
     * HTTP POST /test
     * Java method: com.gitlab.croclabs.react_server.controller.TestController.testPost
     */
    testPost(): RestResponse<TestDto> {
        return this.httpClient.request({ method: "POST", url: uriEncoding`test` });
    }

    /**
     * HTTP GET /test/lazy
     * Java method: com.gitlab.croclabs.react_server.controller.TestController.testLazy
     */
    testLazy(): RestResponse<TestDto> {
        return this.httpClient.request({ method: "GET", url: uriEncoding`test/lazy` });
    }

    /**
     * HTTP GET /test404
     * Java method: com.gitlab.croclabs.react_server.controller.TestController.test404
     */
    test404(): RestResponse<TestDto> {
        return this.httpClient.request({ method: "GET", url: uriEncoding`test404` });
    }
}

export type RestResponse<R> = Promise<R>;

function uriEncoding(template: TemplateStringsArray, ...substitutions: any[]): string {
    let result = "";
    for (let i = 0; i < substitutions.length; i++) {
        result += template[i];
        result += encodeURIComponent(substitutions[i]);
    }
    result += template[template.length - 1];
    return result;
}
