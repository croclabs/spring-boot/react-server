import axios from "axios";
import { HttpClient, RestApplicationClient, RestResponse } from "java/types";

class HttpClientImpl implements HttpClient {
    request<R>(requestConfig: { method: string; url: string; queryParams?: any; data?: any; copyFn?: ((data: R) => R) | undefined; }): RestResponse<R> {
        return axios.request<R>({
            method: requestConfig.method,
            url: "/" + requestConfig.url,
            params: requestConfig.queryParams,
            data: requestConfig.data
        }).then(res => res.data);
    }
}

const rest = new RestApplicationClient(new HttpClientImpl());
export default rest;