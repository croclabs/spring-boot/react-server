import translationEN from 'locales/en/translation.json';
import translationDE from 'locales/de/translation.json';

function translation(t: any) {
    return {
        translation: t
    }
}

const locales =  {
    en: translation(translationEN),
    de: translation(translationDE)
}

export default locales;