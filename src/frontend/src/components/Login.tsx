import { Box, Button, TextField, Typography } from "@mui/material";
import axios, { AxiosResponse } from "axios";
import 'css/Login.scss'
import { FormEvent, MouseEvent, useState } from "react";
import { Id, toast } from "react-toastify";
import { UserDto } from "java/types";
import User from "data/User";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faDiscord, faGoogle, faFacebook } from "@fortawesome/free-brands-svg-icons";
import { useTranslation } from "react-i18next";
import { TFunction } from "i18next";

type LoginDataKey = keyof LoginData;

interface LoginState {
    data: LoginData
    disabled: boolean
    errors: LoginErrors
}

interface LoginData {
    username: string
    password: string
}

interface LoginErrors {
    usernameError: boolean
    passwordError: boolean
}

function initState(oldState?: LoginState) : LoginState {
    let newState: LoginState = {
        data: {
            username: '',
            password: ''
        },
        errors: {
            usernameError: false,
            passwordError: false
        },
        disabled: true
    }

    if (oldState != null) {
        newState.data.username = oldState.data.username;
        newState.data.password = oldState.data.password;
        newState.disabled = oldState.disabled;
        newState.errors.usernameError = oldState.errors.usernameError;
        newState.errors.passwordError = oldState.errors.passwordError;
    }

    return newState;
}

function checkValue(value: string): boolean {
    return value.trim() !== '';
}

function checkInput(inputName: LoginDataKey, state: LoginState, value: string): LoginState {
    let errorName = inputName + 'Error' as keyof LoginErrors
    let newState = initState(state)
    newState.data[inputName] = value.trim();
    let check = checkValue(value.trim());

    newState.errors[errorName] = !check;

    if (newState.data.username.trim() === '' || newState.data.password.trim() === '') {
        newState.disabled = true;
    } else {
        newState.disabled = false;
    }

    return newState;
}

function login(loginData: LoginData, t: TFunction<"translation", undefined>) {
    const params = new URLSearchParams();
    params.append('username', loginData.username);
    params.append('password', loginData.password);

    const ref = toast.loading(t('login.loading'));

    axios.post<UserDto>('/login', params)
        .then(loginSuccess)
        .catch(() => loginFail(ref, t));
}

function loginSuccess(res: AxiosResponse<UserDto, any>) {
    User.current(res.data);
    window.location.href  = '/'
}

function loginFail(ref: Id, t: TFunction<"translation", undefined>) {
    console.error('Login failed!');
    toast.update(ref, {
        render: t('login.error'),
        type: 'error',
        isLoading: false,
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        draggable: true
    });
}

export default function Login() {
    const { t } = useTranslation();
    const [state, setState] = useState<LoginState>(initState());

    const inputName = (event: FormEvent<HTMLInputElement | HTMLTextAreaElement>) => setState(checkInput('username', state, event.currentTarget.value))
    const inputPassword = (event: FormEvent<HTMLInputElement | HTMLTextAreaElement>) => setState(checkInput('password', state, event.currentTarget.value))
    const doLogin = (event: MouseEvent<HTMLButtonElement>) => {
        event.stopPropagation();
        event.preventDefault();
        login(state.data, t);
    }

    return (
        <>
            <form className="login-wrapper">
                <Box className="login-inputs">
                    <Typography className="login-header">{t('login.title')}</Typography>
                    <TextField name="username" error={state.errors.usernameError} value={state.data.username} inputProps={{onInput: inputName}} required id="outlined-basic" label={t('login.username')} variant="outlined" />
                    <TextField name="password" error={state.errors.passwordError} value={state.data.password} inputProps={{onInput: inputPassword}} required type="password" id="outlined-basic" label={t('login.password')} variant="outlined" />
                    <Button type="submit" disabled={state.disabled} className="login-button" variant="contained" onClick={doLogin}>{t('login.button')}</Button>
                    <Button className="login-button" variant="text">{t('login.register')}</Button>
                    <hr></hr>
                    <Typography className="login-header">{t('login.with')}</Typography>
                    <Button className="login-button oauth google" variant="outlined"><FontAwesomeIcon size="lg" icon={faGoogle}/> Google</Button>
                    <Button className="login-button oauth facebook" variant="outlined"><FontAwesomeIcon size="lg" icon={faFacebook}/> Facebook</Button>
                    <Button className="login-button oauth discord" variant="outlined"><FontAwesomeIcon size="lg" icon={faDiscord}/> Discord</Button>
                </Box>
            </form>
        </>
    )
}