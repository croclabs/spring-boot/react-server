import rest from "http/Http";
import Lazy from "components/LazyComponent";
import TestComponent from "components/TestComponent";

function TestLazyComponent() {
    return(
        <Lazy
            renderer={TestComponent}
            loader={rest.testLazy()}
        />
    )
}

export default TestLazyComponent;