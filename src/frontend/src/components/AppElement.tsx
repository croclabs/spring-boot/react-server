import logo from 'logo.svg';
import 'App.scss';
import { Outlet } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import { ChangeEvent } from 'react';
import TestLazyComponent from 'components/TestLazyComponent';
import User from 'data/User';

interface AppElementProps {
    text: string
}

function App(props: AppElementProps) {
    const { t, i18n } = useTranslation();

    const changeLanguage = (event: ChangeEvent<HTMLSelectElement>) => i18n.changeLanguage(event.target.value);

    let lang = localStorage.getItem('i18nextLng') ?? 'en';

    return (
        <div className="App">
          <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <select value={lang} onChange={changeLanguage}>
              <option value={"de"}>DE</option>
              <option value={"en"}>EN</option>
            </select>
            <TestLazyComponent></TestLazyComponent>
            <p>
              {t('welcome.text')} {User.current()?.username} Edit <code>src/App.tsx</code> and save to reload. {props.text}
            </p>

            <Outlet/>
            
            <a
              className="App-link"
              href="https://reactjs.org"
              target="_blank"
              rel="noopener noreferrer"
            >
              Learn React
            </a>
          </header>
        </div>
    );
  }
  
  export default App;