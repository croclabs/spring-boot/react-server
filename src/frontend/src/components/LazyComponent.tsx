import { useEffect, useState } from "react";

interface LazyProps<T> {
    renderer(obj: T): React.ReactNode,
    loader: Promise<T>,
    placeholder?: React.ReactNode
}

function Lazy<T>(props: LazyProps<T>) {
    const [renderer, setRenderer] = useState<React.ReactNode | null>(null);
    const l = props.loader;
    const r = props.renderer;

    useEffect(() => {
        l.then((data) => {
            setRenderer(r(data))
        })
    }, [l, r])

    return(
        <>
            {renderer ?? (props.placeholder ?? <div>Loading...</div>)}
        </>
    )
}

export default Lazy