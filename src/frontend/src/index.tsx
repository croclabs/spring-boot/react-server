import ReactDOM from 'react-dom/client';
import 'index.css';
import App from 'App';
import reportWebVitals from 'reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import axios from 'axios';
import csrf from 'data/CSRF';

axios.defaults.headers.post[csrf.headerName] = csrf.token;
axios.defaults.headers.patch[csrf.headerName] = csrf.token;
axios.defaults.headers.put[csrf.headerName] = csrf.token;
axios.defaults.headers.delete[csrf.headerName] = csrf.token;

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
