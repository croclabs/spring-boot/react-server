interface CsrfToken {
    parameterName: string;
    headerName: string;
    token: string;
}

declare global {
    interface Window { csrf: CsrfToken; }
}

const csrf = window.csrf;
export default csrf;