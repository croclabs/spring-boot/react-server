import axios from "axios";
import { UserDto } from "java/types";
import rest from "http/Http";

export default class User {
    private static user?: UserDto;

    static current(user?: UserDto): UserDto | undefined {
        if (user != null) {
            User.user = user;
            localStorage.setItem('user', JSON.stringify(user));
            return user;
        }

        if (User.user != null) {
            return User.user;
        }

        let sessionUser = localStorage.getItem('user');
        
        if (sessionUser != null) {
            try {
                User.user = JSON.parse(sessionUser);
            } catch {
                console.error('User could not be parsed')
                User.logout();
            }
        } else {
            console.error('No User')
            User.logout();
        }

        return User.user;
    }

    static async update(): Promise<UserDto | undefined> {
        return rest.currentUser()
            .then(user => User.current(user));
    }

    static logout() {
        axios.post('/logout')
            .then(() => {
                localStorage.removeItem('user')
                window.location.replace('/login')
            })
            .catch((e) => console.error(e));
    }
}