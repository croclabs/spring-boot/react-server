import rest from 'http/Http';
import { Route, Routes } from 'react-router-dom';
import AppElement from 'components/AppElement';

import 'i18n';
import Login from 'components/Login';
import { Flip, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css'

function App() {

  rest.test().then(text => console.log(text));
  rest.test404().then(text => console.log(text)).catch(e => console.error(404));
  rest.testPost().then(text =>  console.log(text));

  return (
    <>
      <Routes>
        <Route path='special' element={<AppElement text='Special Path'/>}>
          <Route path='sub' element={<p>Sub</p>}/>
        </Route>
        <Route path='error/500' element={<>500 Internal Server Error</>}/>
        <Route path='special/sub2' element={<AppElement text='Special Sub 2 Path'/>}/>
        <Route path='login' element={<Login/>}/>
        <Route path='*' element={<AppElement text='Normal Path'/>}/>
      </Routes>
      
      <ToastContainer
        position="bottom-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored"
        transition={Flip}
      />
    </>
  );
}

export default App;
